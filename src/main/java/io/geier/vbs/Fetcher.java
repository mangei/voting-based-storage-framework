/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import java.util.List;
import java.util.Map;

/**
 * Fetching interface for retrieving data objects.
 *
 * @param <TMetaInfo> meta-information class
 * @param <TDataObject> data object class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 */
public interface Fetcher<TMetaInfo, TDataObject> {

    /**
     * Retrieves a data object based by its meta-information object.
     * If no data object was found, <code>null</code> will be returned.
     * @param metaInfo meta-information object of the data object to retrieve
     * @return data object or <code>null</code>
     */
    TDataObject fetch(TMetaInfo metaInfo);

    /**
     * Retrieves multiple data objects by its meta-information objects within a map. The key is the meta-information
     * object and the value is the corresponding data object.
     * If no data object was found the value will be <code>null</code>.
     * @param metaInfos meta-information objects of the data objects to retrieve
     * @return map of meta-information objects to data objects
     */
    Map<TMetaInfo, TDataObject> fetchAll(List<TMetaInfo> metaInfos);
}
