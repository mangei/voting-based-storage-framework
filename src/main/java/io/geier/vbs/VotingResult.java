/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a container for a request of a meta-information object, to retrieve a data object.
 *
 * @param <TMetaInfo> meta-information class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 */
public class VotingResult<TMetaInfo> {
    private TMetaInfo metaInfo;
    private long yesCount;
    private long noCount;
    private long noneCount;
    private List<Vote> votes;

    VotingResult(TMetaInfo metaInfo) {
        this.metaInfo = metaInfo;
        this.yesCount = 0;
        this.noCount = 0;
        this.noneCount = 0;
        this.votes = new ArrayList<Vote>();
    }

    /**
     * Counts a new vote to all votes
     * @param vote new vote
     */
    void count(Vote vote) {
        if(vote == null) {
            return;
        }
        switch (vote.getType()) {
            case YES:
                yesCount += vote.getMultiplier();
                break;
            case NO:
                noCount += vote.getMultiplier();
                break;
            case NONE:
                noneCount += vote.getMultiplier();
                break;
        }
        votes.add(vote);
    }

    /**
     * Returns the vote type which has the most votes. If there is a draw <code>VoteType.NONE</code> will be returned.
     * @return highest vote type, or <code>VoteType.NONE</code>
     */
    public VoteType getHighestVote() {
        VoteType highestVote = VoteType.NONE;
        if (yesCount > noCount) {
            highestVote = VoteType.YES;
        } else if (noCount > yesCount) {
            highestVote = VoteType.NO;
        }
        return highestVote;
    }

    /**
     * Checks if the highest vote was a specific vote type.
     * @param voteType specific vote type
     * @return <code>true</code> if the specified vote type is the type with the most votes, otherwise <code>false</code>
     */
    public boolean isHighestVote(VoteType voteType) {
        return getHighestVote() == voteType;
    }

    /**
     * Returns the meta-information object for this voting result
     * @return meta-information object for this voting result
     */
    public TMetaInfo getMetaInfo() {
        return metaInfo;
    }

    /**
     * Returns the number of  votes, where the vote type was YES.
     * @return number of YES votes
     */
    public long getYesCount() {
        return yesCount;
    }

    /**
     * Returns the number of  votes, where the vote type was NO.
     * @return number of NO votes
     */
    public long getNoCount() {
        return noCount;
    }

    /**
     * Returns the number of  votes, where the vote type was NONE.
     * @return number of NONE votes
     */
    public long getNoneCount() {
        return noneCount;
    }

    /**
     * Returns a list of all votes
     * @return list of all votes
     */
    public List<Vote> getVotes() {
        return votes;
    }

    @Override
    public String toString() {
        return "votes: YES: " + yesCount + ", NO: " + noCount + ", NONE: " + noneCount;
    }
}
