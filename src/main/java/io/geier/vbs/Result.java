/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

/**
 * This is a container for a request of a meta-information object, to retrieve a data object.
 *
 * @param <TMetaInfo> meta-information class
 * @param <TDataObject> data object class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 */
public class Result<TMetaInfo, TDataObject> {

    private TMetaInfo metaInfo;
    private TDataObject dataObject;
    private VotingResult<TMetaInfo> votingResult;

    Result(TMetaInfo metaInfo, TDataObject dataObject, VotingResult votingResult) {
        this.metaInfo = metaInfo;
        this.dataObject = dataObject;
        this.votingResult = votingResult;
    }

    /**
     * Returns the meta-information object of for the result
     * @return meta-information object
     */
    public TMetaInfo getMetaInfo() {
        return metaInfo;
    }

    /**
     * Returns the data object as the result for the meta-information object
     * @return data object
     */
    public TDataObject getDataObject() {
        return dataObject;
    }

    /**
     * Returns the voting result of the voting process. This is null if there was no voting process which means that
     * the action was forced.
     * @return voting result
     */
    public VotingResult<TMetaInfo> getVotingResult() {
        return votingResult;
    }
}
