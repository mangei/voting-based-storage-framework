/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

/**
 * Interface for a voter implementation.
 *
 * @param <TMetaInfo> meta-information class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 */
public interface Voter<TMetaInfo> {

    /**
     * Vote if the data object should be stored on a get call (<code>get</code>, <code>getAll</code>)
     * @param metaInfo meta-information object of the data object
     * @param dataContainer data container
     * @return a vote
     */
    Vote shouldStoreOnGet(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer);

    /**
     * Vote if the data object should be preloaded (<code>preload</code>,
     * <code>preloadAll</code>)
     * @param metaInfo meta-information object of the data object
     * @param dataContainer data container
     * @return a vote
     */
    Vote shouldPreload(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer);

    /**
     * Vote if the data object should be updated on a update call (<code>update</code>,
     * <code>udpateAll</code>)
     * @param metaInfo meta-information object of the data object
     * @param dataContainer data container
     * @return a vote
     */
    Vote shouldUpdate(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer);

    /**
     * Vote if the data object should be removed on a remove call (<code>remove</code>,
     * <code>removeAll</code>, <code>clean</code>)
     * @param metaInfo meta-information object of the data object
     * @param dataContainer data container
     * @return a vote
     */
    Vote shouldRemove(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer);
}
