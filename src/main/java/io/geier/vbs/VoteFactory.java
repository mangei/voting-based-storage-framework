/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

/**
 * This class provides factory methods to create <code>Vote</code> objects.
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 *
 * @see Vote
 */
public class VoteFactory {

    public static Vote create(Voter voter, VoteType voteType, int multiplier, String reason) {
        return new Vote(voter, voteType, multiplier, reason);
    }

    public static Vote NONE(Voter voter) {
        return new Vote(voter, VoteType.NONE, Vote.DEFAULT_MULTIPLIER, null);
    }

    public static Vote YES(Voter voter) {
        return new Vote(voter, VoteType.YES, Vote.DEFAULT_MULTIPLIER, null);
    }

    public static Vote YES(Voter voter, String reason) {
        return new Vote(voter, VoteType.YES, Vote.DEFAULT_MULTIPLIER, reason);
    }

    /**
     * The multiplier defines the weight of the vote compared to other votes. The value must be
     * an positive integer. The default value should be 1.
     *
     * @return multiplier factor >= 1
     */
    public static Vote YES(Voter voter, int multiplier) {
        return new Vote(voter, VoteType.YES, multiplier, null);
    }

    public static Vote YES(Voter voter, int multiplier, String reason) {
        return new Vote(voter, VoteType.YES, multiplier, reason);
    }

    public static Vote NO(Voter voter) {
        return new Vote(voter, VoteType.NO, Vote.DEFAULT_MULTIPLIER, null);
    }

    public static Vote NO(Voter voter, String reason) {
        return new Vote(voter, VoteType.NO, Vote.DEFAULT_MULTIPLIER, reason);
    }

    /**
     * The multiplier defines the weight of the vote compared to other votes. The value must be
     * an positive integer. The default value should be 1.
     *
     * @return multiplier factor >= 1
     */
    public static Vote NO(Voter voter, int multiplier) {
        return new Vote(voter, VoteType.NO, multiplier, null);
    }

    public static Vote NO(Voter voter, int multiplier, String reason) {
        return new Vote(voter, VoteType.NO, multiplier, reason);
    }
}
