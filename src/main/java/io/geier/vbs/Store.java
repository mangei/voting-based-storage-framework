/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import java.util.List;
import java.util.Map;

/**
 * This interface represents a client side store. This could be a local database, a internal cache, a local
 * file system or any other store, where data can be stored. Based on its meta-information object, data objects
 * needs to be managed by the store.
 *
 * @param <TMetaInfo> meta-information class
 * @param <TDataObject> data object class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 */
public interface Store<TMetaInfo, TDataObject> {

    /**
     * Returns a data object based on its meta-information object. If the data object could not be found, the value
     * will be <code>null</code>.
     * @param metaInfo meta-information object of the data object to be retrieved
     * @return data object or <code>null</code>
     */
    TDataObject get(TMetaInfo metaInfo);

    /**
     * Returns a map entries. The meta-information object is the key and its data object is the value of the map. If
     * a data object is not found, the value will be <code>null</code>.
     * @param metaInfos list of meta-information objects which should be retrieved
     * @return map of meta-information objects to data objects
     */
    Map<TMetaInfo, TDataObject> getAll(List<TMetaInfo> metaInfos);

    /**
     * Returns a list of all meta-information objects which are stored in the store.
     * @return list of all meta-information objects
     */
    List<TMetaInfo> getAllMetaInfos();

    /**
     * Check if a specific entry is stored in the store. The check will be done by its meta-information object.
     * @param metaInfo meta-information object to be checked
     * @return returns <code>true</code>, if the object is in the store, otherwise <code>false</code>
     */
    boolean containsMetaInfo(TMetaInfo metaInfo);

    /**
     * Check if some entries are stored in the store. The check will be done by its meta-information object.
     * @param metaInfos list of meta-information objects to be checked
     * @return returns a map of meta-information objects to <code>Boolean.TRUE</code> or <code>Boolean.FALSE</code>
     */
    Map<TMetaInfo, Boolean> containsMetaInfos(List<TMetaInfo> metaInfos);

    /**
     * Stores an entry in the store. If the entry - based on its meta-information object - is already stored, the
     * data object will be overwritten.
     * @param metaInfo meta-information object of the data object
     * @param dataObject data object to be stored
     */
    void store(TMetaInfo metaInfo, TDataObject dataObject);

    /**
     * Stores a list of entries in the store. If an entry - based on its meta-information object - is already stored,
     * the data object will be overwritten.
     * @param entries a map of entries which should be stored
     */
    void storeAll(Map<TMetaInfo, TDataObject> entries);

    /**
     * Removes an entry from the store
     * @param metaInfo meta-information object of the entry which should be removed
     */
    void remove(TMetaInfo metaInfo);

    /**
     * Removes multiple entries from the store
     * @param metaInfos meta-information objects of the entries which should be removed
     */
    void removeAll(List<TMetaInfo> metaInfos);

    /**
     * Returns the number of entries which are in the store
     * @return number of stored entries
     */
    long size();
}
