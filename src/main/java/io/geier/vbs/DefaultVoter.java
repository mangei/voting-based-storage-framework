/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

/**
 * Default implementation of the <code>Voter</code> interface. You can use this, if you only want to overwrite some
 * specific voting methods of the <code>Voter</code> interface.
 *
 * @param <TMetaInfo> meta-information class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 *
 * @see Voter
 */
public abstract class DefaultVoter<TMetaInfo> implements Voter<TMetaInfo> {

    @Override
    public Vote shouldStoreOnGet(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer) {
        return VoteFactory.NONE(getVoter());
    }

    @Override
    public Vote shouldPreload(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer) {
        return VoteFactory.NONE(getVoter());
    }

    @Override
    public Vote shouldUpdate(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer) {
        return VoteFactory.NONE(getVoter());
    }

    @Override
    public Vote shouldRemove(TMetaInfo metaInfo, DataContainer<TMetaInfo, ?> dataContainer) {
        return VoteFactory.NONE(getVoter());
    }

    /**
     * Should return the class which uses this voter.
     * Usually <code>this</code>, if the class was extended from the <code>DefaultVoter</code>.
     * @return voter class (usually <code>this</code>)
     */
    protected abstract Voter<TMetaInfo> getVoter();
}
