/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

/**
 * This class represents a vote. To create a vote use the <code>VoteFactory</code>
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 *
 * @see VoteFactory
 */
public class Vote {

    public static int DEFAULT_MULTIPLIER = 1;

    private Voter voter;
    private VoteType type;
    private int multiplier;
    private String reason;

    Vote(Voter voter, VoteType voteType, int multiplier, String reason) {
        if(voter == null) throw new IllegalArgumentException("voter must not be null");
        if(voteType == null) throw new IllegalArgumentException("voteType must not be null");
        if(multiplier < 1) throw new IllegalArgumentException("multiplier must be >= 1");
        this.voter = voter;
        this.type = voteType;
        this.multiplier = multiplier;
        this.reason = reason;
    }

    /**
     * Returns the voter object which created this vote
     * @return voter for this vote
     */
    public Voter getVoter() {
        return voter;
    }

    /**
     * Returns the type of the vote
     * @return type of the vote
     */
    public VoteType getType() {
        return type;
    }

    /**
     * Returns the multiplier value for this vote
     * @return multiplier value for this vote
     */
    public int getMultiplier() {
        return multiplier;
    }

    /**
     * Returns the reason for the vote
     * @return reason fo the vote
     */
    public String getReason() {
        return reason;
    }
}
