/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a data container for objects.
 * It will trigger the voting process with all Voter objects for different operations, use the Fetcher if the object
 * is not in the store and needs to be received and stores, updated and removes objects from the Store.
 *
 * @param <TMetaInfo> meta-information class
 * @param <TDataObject> data object class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 */
public class DataContainer<TMetaInfo, TDataObject> {

    private static Logger LOG = LoggerFactory.getLogger(DataContainer.class);

    private Store<TMetaInfo, TDataObject> store;
    private Fetcher<TMetaInfo, TDataObject> fetcher;
    private List<Voter<TMetaInfo>> voters;

    public DataContainer(Store<TMetaInfo, TDataObject> store, Fetcher<TMetaInfo, TDataObject> fetcher, List<Voter<TMetaInfo>> voters) {
        if(store == null) throw new IllegalArgumentException("store must not be null");
        if(fetcher == null) throw new IllegalArgumentException("fetcher must not be null");
        if(voters == null) throw new IllegalArgumentException("voters must not be null");
        this.store = store;
        this.fetcher = fetcher;
        this.voters = voters;
    }

    /**
     * Returns the number of entries which are in the data container
     * @return number of entries
     */
    public long size() {
        return store.size();
    }

    /**
     * Returns the internal store object of the data container
     * @return store object of the data container
     */
    public Store<TMetaInfo, TDataObject> getStore() {
        return store;
    }

    /**
     * Returns the internal fetcher object of the data container
     * @return fetcher object of the data container
     */
    public Fetcher<TMetaInfo, TDataObject> getFetcher() {
        return fetcher;
    }

    /**
     * Returns the internal voters object of the data container
     * @return list of all voter objects of the data container
     */
    public List<Voter<TMetaInfo>> getVoters() {
        return voters;
    }

    /**
     * Returns the object from the Store or fetches it by the Fetcher. If it gets fetched a voting process will be
     * started, if the data object should be stored in the store.
     *
     * @param metaInfo meta-information object
     * @return result object
     */
    public Result<TMetaInfo, TDataObject> get(TMetaInfo metaInfo) {
        return get(metaInfo, false);
    }

    /**
     * Returns the object from the Store or fetches it by the Fetcher. If it gets fetched and forceStore is false a
     * voting process will be started, if the data object should be stored in the store. If forceStore is true, it will
     * be stored after the the fetch process.
     *
     * @param metaInfo meta-information object
     * @param forceStore flag if the fetched data object should be force stored without voting
     * @return results object
     */
    public Result<TMetaInfo, TDataObject> get(TMetaInfo metaInfo, boolean forceStore) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");
        TDataObject dataObject = store.get(metaInfo);
        Result<TMetaInfo, TDataObject> result;
        if (dataObject != null) {
            result = new Result<TMetaInfo, TDataObject>(metaInfo, dataObject, null);
            LOG.info("data from store: " + metaInfo.toString());
        } else {
            result = fetchAndMayStore(metaInfo, forceStore);
        }
        return result;
    }

    /**
     * Returns a list of objects from the Store or fetched it by the Fetcher. If it gets fetched a voting process will
     * be started, if the data object should be stored in the store.
     *
     * @param metaInfos meta-information objects
     * @return results object
     */
    public Results<TMetaInfo, TDataObject> getAll(List<TMetaInfo> metaInfos) {
        return getAll(metaInfos, false);
    }

    /**
     * Returns the object from the Store or fetches it by the Fetcher. If it gets fetched and forceStore is false a
     * voting process will be started, if the data object should be stored in the store. If forceStore is true, it will
     * be stored after the the fetch process.
     *
     * @param metaInfos meta-information objects
     * @param forceStore flag if the fetched data objects should be force stored without voting
     * @return results object
     */
    public Results<TMetaInfo, TDataObject> getAll(List<TMetaInfo> metaInfos, boolean forceStore) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        Results<TMetaInfo, TDataObject> results = new Results<TMetaInfo, TDataObject>();

        // get all objects from the store
        Map<TMetaInfo, TDataObject> storedEntries = store.getAll(metaInfos);

        // iterate through all entries and add it them to the results
        // or add them to the entry list for objects which need to be fetched
        List<TMetaInfo> entriesToBeFetched = new ArrayList<TMetaInfo>();
        for(Map.Entry<TMetaInfo, TDataObject> entry : storedEntries.entrySet()) {
            TMetaInfo metaInfo = entry.getKey();
            TDataObject dataObject = entry.getValue();
            if(dataObject != null) {
                // add all result for objects which came from the store
                results.addResult(new Result<TMetaInfo, TDataObject>(metaInfo, dataObject, null));
            } else {
                // add the entry to the entries which should be fetched
                entriesToBeFetched.add(metaInfo);
            }
        }

        // try to fetch all missing entries and add the results to the results from objects which came from the store
        Results<TMetaInfo, TDataObject> fetchResults = fetchAndMayStoreAll(entriesToBeFetched, forceStore, new ShouldAllCondition<TMetaInfo>() {
            @Override
            public VotingResults<TMetaInfo> shouldAllCondition(List<TMetaInfo> list) {
                return shouldStoreOnGetAll(list);
            }
        });
        results.addResults(fetchResults);

        return results;
    }

    private Result<TMetaInfo, TDataObject> fetchAndMayStore(TMetaInfo metaInfo, boolean forceStore) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");
        TDataObject dataObject = fetch(metaInfo);
        VotingResult<TMetaInfo> votingResult = null;
        if(dataObject != null) {
            if(forceStore) {
                store(metaInfo, dataObject);
            } else {
                votingResult = shouldStoreOnGet(metaInfo);
                if (votingResult.isHighestVote(VoteType.YES)) {
                    store(metaInfo, dataObject);
                }
            }
        }
        return new Result<TMetaInfo, TDataObject>(metaInfo, dataObject, votingResult);
    }

    private Results<TMetaInfo, TDataObject> fetchAndMayStoreAll(List<TMetaInfo> metaInfos, boolean forceStore, ShouldAllCondition<TMetaInfo> shouldStoreAllCondition) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        Results<TMetaInfo, TDataObject> results = new Results<TMetaInfo, TDataObject>();

        // fetch all entries
        Map<TMetaInfo, TDataObject> fetchedObjects = fetchAll(metaInfos);

        // get all entries, where we received a data object
        List<TMetaInfo> mayStoreObjects = new ArrayList<TMetaInfo>();
        for(Map.Entry<TMetaInfo, TDataObject> fetchedObject : fetchedObjects.entrySet()) {
            TMetaInfo metaInfo = fetchedObject.getKey();
            TDataObject dataObject = fetchedObject.getValue();
            if(dataObject != null) {
                mayStoreObjects.add(metaInfo);
            } else {
                // add all objects which could not be fetched (data object is null)
                results.addResult(new Result<TMetaInfo, TDataObject>(metaInfo, dataObject, null));
            }
        }

        // get all objects which should be stored
        Map<TMetaInfo, TDataObject> storeObjects = new HashMap<TMetaInfo, TDataObject>();
        if(forceStore) {
            // we want to store all object which got fetched
            for(TMetaInfo metaInfo : mayStoreObjects) {
                TDataObject dataObject = fetchedObjects.get(metaInfo);
                storeObjects.put(metaInfo, dataObject);

                // add all results without voting
                results.addResult(new Result<TMetaInfo, TDataObject>(metaInfo, dataObject, null));
            }
        } else {
            // we vote for all fetched objects and only those with positive results should be stored
            VotingResults<TMetaInfo> shouldStoreResults = shouldStoreAllCondition.shouldAll(mayStoreObjects);
            for(Map.Entry<TMetaInfo, VotingResult<TMetaInfo>> entry : shouldStoreResults.getVotingResults().entrySet()) {
                TMetaInfo metaInfo = entry.getKey();
                VotingResult<TMetaInfo> votingResult = entry.getValue();
                TDataObject dataObject = fetchedObjects.get(metaInfo);
                if(votingResult.isHighestVote(VoteType.YES)) {
                    storeObjects.put(metaInfo, dataObject);
                }

                // add all results with voting
                results.addResult(new Result<TMetaInfo, TDataObject>(metaInfo, dataObject, votingResult));
            }
        }

        // store all object which should to be stored
        storeAll(storeObjects);

        return results;
    }

    private TDataObject fetchAndStore(TMetaInfo metaInfo) {
        TDataObject dataObject = fetch(metaInfo);
        store(metaInfo, dataObject);
        return dataObject;
    }

    private Map<TMetaInfo, TDataObject> fetchAndStoreAll(List<TMetaInfo> metaInfos) {
        return fetchAndStoreAll(metaInfos, false);
    }

    private Map<TMetaInfo, TDataObject> fetchAndStoreAll(List<TMetaInfo> metaInfos, boolean forceStore) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        Map<TMetaInfo, TDataObject> entries = fetchAll(metaInfos);
        if(forceStore) {
            storeAll(entries);
        } else {
            VotingResults<TMetaInfo> votingResults = shouldStoreOnGetAll(metaInfos);
            Map<TMetaInfo, VotingResult<TMetaInfo>> positiveVotingResults = votingResults.getVotingResultsByVoteType(VoteType.YES);
            Map<TMetaInfo, TDataObject> entriesToStore = new HashMap<TMetaInfo, TDataObject>();
            for(TMetaInfo metaInfo : positiveVotingResults.keySet()) {
                entriesToStore.put(metaInfo, entries.get(metaInfo));
            }
            storeAll(entriesToStore);
        }
        return entries;
    }

    private VotingResult<TMetaInfo> mayFetchAndStore(TMetaInfo entryToFetch, ShouldCondition<TMetaInfo> shouldFetchCondition) {
        // vote vor all entries
        VotingResult<TMetaInfo> votingResult = shouldFetchCondition.should(entryToFetch);

        // fetch and force store entry which should be fetched
        if(votingResult.isHighestVote(VoteType.YES)) {
            fetchAndMayStore(entryToFetch, true);
        }

        return votingResult;
    }

    private VotingResults<TMetaInfo> mayFetchAndStoreAll(List<TMetaInfo> entriesToFetch, ShouldAllCondition<TMetaInfo> shouldFetchAllCondition) {
        // vote vor all entries
        VotingResults<TMetaInfo> votingResults = shouldFetchAllCondition.shouldAll(entriesToFetch);

        // fetch and force store all entries which should be fetched
        List<TMetaInfo> entriesToPrefetch = new ArrayList<TMetaInfo>();
        for(TMetaInfo metaInfo : votingResults.getVotingResultsByVoteType(VoteType.YES).keySet()) {
            entriesToPrefetch.add(metaInfo);
        }
        fetchAndMayStoreAll(entriesToPrefetch, true, null);

        return votingResults;
    }

    private TDataObject fetch(TMetaInfo metaInfo) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");
        LOG.info("data fetching: " + metaInfo.toString());
        TDataObject dataObject = fetcher.fetch(metaInfo);
        LOG.info("data fetched: " + dataObject);
        return dataObject;
    }

    private Map<TMetaInfo,TDataObject> fetchAll(List<TMetaInfo> metaInfos) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        Map<TMetaInfo,TDataObject> dataObjects = new HashMap<TMetaInfo,TDataObject>();
        if(!metaInfos.isEmpty()) {
            dataObjects = fetcher.fetchAll(metaInfos);
        }
        return dataObjects;
    }

    private void store(TMetaInfo metaInfo, TDataObject dataObject) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");
        store.store(metaInfo, dataObject);
        LOG.info("data stored: " + metaInfo.toString());
    }

    private void storeAll(Map<TMetaInfo,TDataObject> entries) {
        if(entries == null) throw new IllegalArgumentException("entries must not be null");
        store.storeAll(entries);
    }

    /**
     * Preloads a data object by its meta-information object, if the voting process is positive.
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> preload(TMetaInfo metaInfo) {
        return preload(metaInfo, false);
    }

    /**
     * Preloads a data object by its meta-information object. If the voting process is positive or the forceStore
     * flag is set to true.
     *
     * @param metaInfo meta-information object
     * @param forceStore flag if the object should be force fetched and stored or voted
     * @return voting result
     */
    public VotingResult<TMetaInfo> preload(TMetaInfo metaInfo, boolean forceStore) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");
        boolean isInStore = store.containsMetaInfo(metaInfo);
        if(forceStore && !isInStore) {
            fetchAndStore(metaInfo);
            return null;
        } else {
            VotingResult<TMetaInfo> votingResult = shouldPreload(metaInfo);
            if(!isInStore && votingResult.isHighestVote(VoteType.YES)) {
                fetchAndStore(metaInfo);
            }
            return votingResult;
        }
    }

    /**
     * Preloads a list of data objects by their meta-information object, if the voting process is positive.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> preloadAll(List<TMetaInfo> metaInfos) {
        return preloadAll(metaInfos, false);
    }

    /**
     * Preloads a list of data objects by their meta-information object. If the voting process is positive or the
     * forceStore flag is set to true.
     *
     * @param metaInfos meta-information objects
     * @param forceStore flag if the objects should be force fetched and stored or voted
     * @return voting results
     */
    public VotingResults<TMetaInfo> preloadAll(List<TMetaInfo> metaInfos, boolean forceStore) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        VotingResults<TMetaInfo> votingResults = new VotingResults<TMetaInfo>();

        // get all entries which are not in the store
        Map<TMetaInfo, Boolean> isInStoreMap = store.containsMetaInfos(metaInfos);
        List<TMetaInfo> notInStoreEntries = new ArrayList<TMetaInfo>();
        for(Map.Entry<TMetaInfo, Boolean> entry : isInStoreMap.entrySet()) {
            TMetaInfo metaInfo = entry.getKey();
            Boolean isInStore = entry.getValue();
            if(!isInStore) {
                notInStoreEntries.add(metaInfo);
            }
        }

        if(forceStore) {
            // fetch and store all entries without voting and add empty voting results
            fetchAndStoreAll(notInStoreEntries, true);
            for(TMetaInfo metaInfo : notInStoreEntries) {
                votingResults.add(metaInfo, null);
            }
        } else {
            // fetches and may stores all entries which are not in the store and sets the voting results
            ShouldAllCondition<TMetaInfo> shouldAll = new ShouldAllCondition<TMetaInfo>() {
                @Override
                public VotingResults<TMetaInfo> shouldAllCondition(List<TMetaInfo> list) {
                    return shouldPreloadAll(list);
                }
            };

            // vote for all entries which are not in store, if the should be prefetched
            // and fetches and stores all entries which should be prefetched
            ShouldAllCondition<TMetaInfo> prefetchAllCondition = new ShouldAllCondition<TMetaInfo>() {
                @Override
                protected VotingResults<TMetaInfo> shouldAllCondition(List<TMetaInfo> list) {
                    return shouldPreloadAll(list);
                }
            };
            VotingResults<TMetaInfo> prefetchVotingResults = mayFetchAndStoreAll(notInStoreEntries, prefetchAllCondition);
            votingResults.addAll(prefetchVotingResults);
        }

        return votingResults;
    }

    /**
     * Update a single data object by its meta-information object
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> update(TMetaInfo metaInfo) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");

        // votes for entry if it should be updated
        // and fetches and stores the entry which should be updated
        ShouldCondition<TMetaInfo> prefetchCondition = new ShouldCondition<TMetaInfo>() {
            public VotingResult<TMetaInfo> should(TMetaInfo metaInfo) {
                return shouldUpdate(metaInfo);
            }
        };
        VotingResult<TMetaInfo> prefetchVotingResult = mayFetchAndStore(metaInfo, prefetchCondition);

        return prefetchVotingResult;
    }

    /**
     * Update a list of data objects by their meta-information objects, if the voting process is positive.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> updateAll(List<TMetaInfo> metaInfos) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        VotingResults<TMetaInfo> votingResults = new VotingResults<TMetaInfo>();

        // votes for all entries if they should be updated
        // and fetches and stores all entries which should be updated
        ShouldAllCondition<TMetaInfo> prefetchAllCondition = new ShouldAllCondition<TMetaInfo>() {
            @Override
            protected VotingResults<TMetaInfo> shouldAllCondition(List<TMetaInfo> list) {
                return shouldUpdateAll(list);
            }
        };
        VotingResults<TMetaInfo> prefetchVotingResults = mayFetchAndStoreAll(metaInfos, prefetchAllCondition);
        votingResults.addAll(prefetchVotingResults);

        return votingResults;
    }

    /**
     * Removes a data object by its meta-information object, if the voting process is positive.
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> remove(TMetaInfo metaInfo) {
        return remove(metaInfo, false);
    }

    /**
     * Removes a data object by its meta-information object. If the voting process is positive or the forceStore
     * flag is set to true.
     *
     * @param metaInfo meta-information object
     * @param forceRemove flag if the object should be force removed or voted
     * @return voting result
     */
    public VotingResult<TMetaInfo> remove(TMetaInfo metaInfo, boolean forceRemove) {
        if(forceRemove) {
            store.remove(metaInfo);
            return null;
        } else {
            VotingResult<TMetaInfo> votingResult = shouldRemove(metaInfo);
            if(votingResult.isHighestVote(VoteType.YES)) {
                store.remove(metaInfo);
            }
            return votingResult;
        }
    }

    /**
     * Remove a list of data objects by their meta-information objects, if the voting process is positive.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> removeAll(List<TMetaInfo> metaInfos) {
        return removeAll(metaInfos, false);
    }

    /**
     * Remove a list of data objects by their meta-information objects. If the voting process is positive or the
     * forceRemove flag is set to true.
     *
     * @param metaInfos meta-information objects
     * param forceRemove flag if the objects should be force removed or voted
     * @return voting results
     */
    public VotingResults<TMetaInfo> removeAll(List<TMetaInfo> metaInfos, boolean forceRemove) {
        if(metaInfos == null) throw new IllegalArgumentException("metaInfos must not be null");
        VotingResults <TMetaInfo> votingResults = new VotingResults<TMetaInfo>();

        if(forceRemove) {
            // remove all without voting and add empty voting results
            if(!metaInfos.isEmpty()) {
                store.removeAll(metaInfos);
            }
            for(TMetaInfo metaInfo : metaInfos) {
                votingResults.add(metaInfo, null);
            }
        } else {
            // get voting results
            votingResults = shouldRemoveAll(metaInfos);

            // get all entries which should be removed
            List<TMetaInfo> entriesToBeRemoved = new ArrayList<TMetaInfo>();
            for(TMetaInfo metaInfo : metaInfos) {
                VotingResult<TMetaInfo> votingResult = votingResults.getVotingResults().get(metaInfo);
                if(votingResult.isHighestVote(VoteType.YES)) {
                    entriesToBeRemoved.add(metaInfo);
                }
                votingResults.add(metaInfo, votingResult);
            }

            // remove all entries which should be removed
            if(!entriesToBeRemoved.isEmpty()) {
                store.removeAll(entriesToBeRemoved);
            }
        }

        return votingResults;
    }

    /**
     * Removes all data objects, if the voting process is positive.
     *
     * @return voting result
     */
    public VotingResults<TMetaInfo> clear() {
        return clear(false);
    }

    /**
     * Removes all data objects. If the voting process is positive or the forceRemove flag is set to true.
     *
     * @param forceRemove flag if the objects should be force removed or voted
     * @return voting result
     */
    public VotingResults<TMetaInfo> clear(boolean forceRemove) {
        // get all meta-information objects
        List<TMetaInfo> metaInfos = store.getAllMetaInfos();
        VotingResults<TMetaInfo> votingResults = removeAll(metaInfos, forceRemove);
        return votingResults;
    }

    /**
     * Do a voting process if the element should be stored on get.
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> shouldStoreOnGet(TMetaInfo metaInfo) {
        LOG.info("should store '" + metaInfo.toString());
        VotingResult<TMetaInfo> shouldStore = should(metaInfo, new VoteResolver<TMetaInfo, TDataObject>() {
            public Vote should(Voter<TMetaInfo> voter, TMetaInfo metaInfo, DataContainer<TMetaInfo, TDataObject> dataContainer) {
                return voter.shouldStoreOnGet(metaInfo, dataContainer);
            }
        });
        LOG.info("should store -> "+shouldStore+" '" + metaInfo.toString());
        return shouldStore;
    }

    /**
     * Do a voting process if the elements should be stored on get.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> shouldStoreOnGetAll(List<TMetaInfo> metaInfos) {
        return shouldAll(metaInfos, new ShouldCondition<TMetaInfo>() {
            public VotingResult<TMetaInfo> should(TMetaInfo metaInfo) {
                return shouldStoreOnGet(metaInfo);
            }
        });
    }

    /**
     * Do a voting process if the element should be preloaded.
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> shouldPreload(TMetaInfo metaInfo) {
        LOG.info("should store '" + metaInfo.toString());
        VotingResult<TMetaInfo> shouldPreload = should(metaInfo, new VoteResolver<TMetaInfo, TDataObject>() {
            public Vote should(Voter<TMetaInfo> voter, TMetaInfo metaInfo, DataContainer<TMetaInfo, TDataObject> dataContainer) {
                return voter.shouldPreload(metaInfo, dataContainer);
            }
        });
        LOG.info("should preload -> " + shouldPreload + " '" + metaInfo.toString());
        return shouldPreload;
    }

    /**
     * Do a voting process if the elements should be preloaded.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> shouldPreloadAll(List<TMetaInfo> metaInfos) {
        return shouldAll(metaInfos, new ShouldCondition<TMetaInfo>() {
            public VotingResult<TMetaInfo> should(TMetaInfo metaInfo) {
                return shouldPreload(metaInfo);
            }
        });
    }

    /**
     * Do a voting process if the element should be updated.
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> shouldUpdate(TMetaInfo metaInfo) {
        LOG.info("should update '" + metaInfo.toString());
        VotingResult<TMetaInfo> shouldUpdate = should(metaInfo, new VoteResolver<TMetaInfo, TDataObject>() {
            public Vote should(Voter<TMetaInfo> voter, TMetaInfo metaInfo, DataContainer<TMetaInfo, TDataObject> dataContainer) {
                return voter.shouldUpdate(metaInfo, dataContainer);
            }
        });
        LOG.info("should update -> "+shouldUpdate+" '" + metaInfo.toString());
        return shouldUpdate;
    }

    /**
     * Do a voting process if the element should be updated.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> shouldUpdateAll(List<TMetaInfo> metaInfos) {
        return shouldAll(metaInfos, new ShouldCondition<TMetaInfo>() {
            public VotingResult<TMetaInfo> should(TMetaInfo metaInfo) {
                return shouldUpdate(metaInfo);
            }
        });
    }

    /**
     * Do a voting process if the element should be removed.
     *
     * @param metaInfo meta-information object
     * @return voting result
     */
    public VotingResult<TMetaInfo> shouldRemove(TMetaInfo metaInfo) {
        LOG.info("should remove '" + metaInfo.toString());
        VotingResult<TMetaInfo> shouldRemove = should(metaInfo, new VoteResolver<TMetaInfo, TDataObject>() {
            public Vote should(Voter<TMetaInfo> voter, TMetaInfo metaInfo, DataContainer<TMetaInfo, TDataObject> dataContainer) {
                return voter.shouldRemove(metaInfo, dataContainer);
            }
        });
        LOG.info("should remove -> "+shouldRemove+" '" + metaInfo.toString());
        return shouldRemove;
    }

    /**
     * Do a voting process if the elements should be removed.
     *
     * @param metaInfos meta-information objects
     * @return voting results
     */
    public VotingResults<TMetaInfo> shouldRemoveAll(List<TMetaInfo> metaInfos) {
        return shouldAll(metaInfos, new ShouldCondition<TMetaInfo>() {
            public VotingResult<TMetaInfo> should(TMetaInfo metaInfo) {
                return shouldRemove(metaInfo);
            }
        });
    }

    private VotingResult<TMetaInfo> should(TMetaInfo metaInfo, VoteResolver<TMetaInfo,TDataObject> voteResolver) {
        VotingResult<TMetaInfo> votingResult = new VotingResult<TMetaInfo>(metaInfo);
        for (Voter<TMetaInfo> voter : voters) {
            Vote vote = voteResolver.should(voter, metaInfo, this);
            votingResult.count(vote);
        }
        LOG.info("store votes: '" + metaInfo.toString() + "': " + votingResult.toString());
        return votingResult;
    }

    private VotingResults<TMetaInfo> shouldAll(List<TMetaInfo> metaInfos, ShouldCondition<TMetaInfo> shouldCondition) {
        VotingResults<TMetaInfo> votingResults = new VotingResults<TMetaInfo>();
        for(TMetaInfo metaInfo : metaInfos) {
            VotingResult<TMetaInfo> votingResult = shouldCondition.should(metaInfo);
            votingResults.add(metaInfo, votingResult);
        }
        return votingResults;
    }

    /**
     * This is a helper class for voting
     * @param <TMetaInfo> meta-information class
     */
    private interface VoteResolver<TMetaInfo, TDataObject> {
        Vote should(Voter<TMetaInfo> voter, TMetaInfo metaInfo, DataContainer<TMetaInfo, TDataObject> dataContainer);
    }

    /**
     * This is a helper class to abstract the should condition
     * @param <TMetaInfo> meta-information class
     */
    private interface ShouldCondition<TMetaInfo> {
        VotingResult<TMetaInfo> should(TMetaInfo metaInfo);
    }

    /**
     * This is a helper class to abstract the shouldAll condition
     * @param <TMetaInfo> meta-information class
     */
    private abstract class ShouldAllCondition<TMetaInfo> {
        VotingResults<TMetaInfo> votingResults;

        /**
         * Returns the results of the voting based on its implementation.
         * @param metaInfos list of meta-information objects
         * @return voting results
         */
        public VotingResults<TMetaInfo> shouldAll(List<TMetaInfo> metaInfos) {
            votingResults = shouldAllCondition(metaInfos);
            return votingResults;
        }

        /**
         * Returns the results of the voting for a specific condition.
         * @param metaInfos list of meta-information objects
         * @return voting results
         */
        abstract protected VotingResults<TMetaInfo> shouldAllCondition(List<TMetaInfo> metaInfos);

        /**
         * Returns the results of the voting, but only if <code>shouldAll</code> was called.
         * @return voting results
         */
        public VotingResults<TMetaInfo> getVotingResults() {
            return votingResults;
        }
    }
}
