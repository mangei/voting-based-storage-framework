/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a container for multiple <code>Result</code> objects.
 *
 * @param <TMetaInfo> meta-information class
 * @param <TDataObject> data object class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 *
 * @see Result
 */
public class Results<TMetaInfo, TDataObject> {

    private List<Result<TMetaInfo, TDataObject>> results;

    Results() {
        this.results = new ArrayList<Result<TMetaInfo, TDataObject>>();
    }

    /**
     * Returns a list of all result objects
     * @return list of all result objects
     */
    public List<Result<TMetaInfo, TDataObject>> getResults() {
        return results;
    }

    /**
     * Add a result to the results
     * @param result result which will be added
     */
    void addResult(Result<TMetaInfo, TDataObject> result) {
        results.add(result);
    }

    /**
     * Add additional results from another results object to the current results
     * @param additionalResults additional results to be added to the current
     */
    void addResults(Results<TMetaInfo, TDataObject> additionalResults) {
        results.addAll(additionalResults.getResults());
    }
}
