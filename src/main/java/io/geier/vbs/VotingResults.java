/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a container for multiple <code>VotingResult</code> objects.
 *
 * @param <TMetaInfo> meta-information class
 *
 * @author Manuel Geier (geier.io)
 * @version 1.0
 *
 * @see VotingResult
 */
public class VotingResults<TMetaInfo> {

    private Map<TMetaInfo, VotingResult<TMetaInfo>> resultMap;
    private Map<VoteType, Map<TMetaInfo, VotingResult<TMetaInfo>>> resultMapByType;

    VotingResults() {
        resultMap = new HashMap<TMetaInfo, VotingResult<TMetaInfo>>();
        resultMapByType = new HashMap<VoteType, Map<TMetaInfo, VotingResult<TMetaInfo>>>();
        for(VoteType voteType : VoteType.values()) {
            resultMapByType.put(voteType, new HashMap<TMetaInfo, VotingResult<TMetaInfo>>());
        }
    }

    /**
     * Add a new voting result.
     * @param metaInfo meta-information object for the result
     * @param votingResult voting result for the meta-information object
     */
    void add(TMetaInfo metaInfo, VotingResult<TMetaInfo> votingResult) {
        if(metaInfo == null) throw new IllegalArgumentException("metaInfo must not be null");
        resultMap.put(metaInfo, votingResult);
        if(votingResult != null) {
            resultMapByType.get(votingResult.getHighestVote()).put(metaInfo, votingResult);
        }
    }

    /**
     * Add a voting results entries to the current voting results object.
     * @param votingResults voting results
     */
    void addAll(VotingResults<TMetaInfo> votingResults) {
        if(votingResults == null) throw new IllegalArgumentException("votingResults must not be null");
        for(Map.Entry<TMetaInfo, VotingResult<TMetaInfo>> entry : votingResults.getVotingResults().entrySet()) {
            TMetaInfo metaInfo = entry.getKey();
            VotingResult<TMetaInfo> votingResult = entry.getValue();
            add(metaInfo, votingResult);
        }
    }

    /**
     * Returns the voting result map with a mapping from each meta-information object to its voting result.
     * @return voting result map
     */
    public Map<TMetaInfo, VotingResult<TMetaInfo>> getVotingResults() {
        return resultMap;
    }

    /**
     * Returns the voting result map with a mapping from each meta-information object to its voting result, where the
     * highest voting result was a specific vote type.
     * @param voteType specific type of the voting result
     * @return voting result map where highest voting results are a specific vote type
     */
    public Map<TMetaInfo, VotingResult<TMetaInfo>> getVotingResultsByVoteType(VoteType voteType) {
        if(voteType == null) throw new IllegalArgumentException("voteType must not be null");
        return resultMapByType.get(voteType);
    }
}
