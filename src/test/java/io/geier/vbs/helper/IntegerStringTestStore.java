/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs.helper;

import io.geier.vbs.Store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a store for all Strings. It also saved the data size of all elements.
 */
public class IntegerStringTestStore implements Store<Integer, String> {

    private Map<Integer, String> store = new HashMap<Integer, String>();

    @Override
    public String get(Integer metaInfo) {
        return store.get(metaInfo);
    }

    @Override
    public Map<Integer, String> getAll(List<Integer> metaInfos) {
        Map<Integer, String> result = new HashMap<Integer, String>();
        for(Integer metaInfo : metaInfos) {
            result.put(metaInfo, store.get(metaInfo));
        }
        return result;
    }

    @Override
    public void store(Integer metaInfo, String dataObject) {
        store.put(metaInfo, dataObject);
    }

    @Override
    public void storeAll(Map<Integer, String> entries) {
        for(Map.Entry<Integer, String> entry : entries.entrySet()) {
            store.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public long size() {
        return store.size();
    }

    @Override
    public List<Integer> getAllMetaInfos() {
        List<Integer> metaInfos = new ArrayList<Integer>();
        for(Integer metaInfo : store.keySet()) {
            metaInfos.add(metaInfo);
        }
        return metaInfos;
    }

    @Override
    public void remove(Integer metaInfo) {
        store.remove(metaInfo);
    }

    @Override
    public void removeAll(List<Integer> metaInfos) {
        for (Integer metaInfo : metaInfos) {
            store.remove(metaInfo);
        }
    }

    @Override
    public boolean containsMetaInfo(Integer metaInfo) {
        return store.containsKey(metaInfo);
    }

    @Override
    public Map<Integer, Boolean> containsMetaInfos(List<Integer> metaInfos) {
        Map<Integer, Boolean> containsMap = new HashMap<Integer, Boolean>();
        for (Integer metaInfo : metaInfos) {
            containsMap.put(metaInfo, new Boolean(containsMetaInfo(metaInfo)));
        }
        return containsMap;
    }
}