/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import io.geier.vbs.helper.IntegerStringTestStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.*;

public class DataContainerTest {

    Store<Integer,String> store;
    Fetcher<Integer,String> fetcher;
    List<Voter<Integer>> voters;
    DataContainer<Integer,String> dataContainer;
    Integer metaInfo1;
    String dataObject1;
    Integer metaInfo2;
    String dataObject2;
    Integer metaInfo3;
    String dataObject3;

    @Before
    public void setUp() {
        store = spy(new IntegerStringTestStore());
        fetcher = mock(Fetcher.class);
        voters = new ArrayList<Voter<Integer>>();
        dataContainer = new DataContainer<Integer,String>(store, fetcher, voters);

        metaInfo1 = 1;
        dataObject1 = "data object 1";
        metaInfo2 = 2;
        dataObject2 = "data object 2";
        metaInfo3 = 3;
        dataObject3 = "data object 3";

        List<Integer> metaInfos = Arrays.asList(metaInfo1, metaInfo2, metaInfo3);

        Map<Integer, String> fetchAllResult = new HashMap<Integer, String>();
        fetchAllResult.put(metaInfo1, dataObject1);
        fetchAllResult.put(metaInfo2, dataObject2);
        fetchAllResult.put(metaInfo3, dataObject3);

        when(fetcher.fetchAll(metaInfos)).thenReturn(fetchAllResult);

        dataContainer.preloadAll(metaInfos, true);

        assertEquals(3, dataContainer.size());
    }

    @After
    public void tearDown() {
        dataContainer = null;
    }

    @Test
    public void get_shouldReturnDataObject() {
        Result<Integer, String> result = dataContainer.get(metaInfo1);

        assertEquals(metaInfo1, result.getMetaInfo());
        assertNotNull(result.getDataObject());
        assertNull(result.getVotingResult());
    }

    @Test
    public void get_shouldReturnNullDataObject() {
        Integer newMetaInfo = 999;

        Result<Integer, String> result = dataContainer.get(newMetaInfo);

        assertEquals(newMetaInfo, result.getMetaInfo());
        assertNull(result.getDataObject());
        assertNull(result.getVotingResult());
    }

    @Test
    public void get_shouldReturnDataObjectByFetchAndStore() {
        Integer newMetaInfo = 999;
        String dataObject = "Vola";
        when(fetcher.fetch(newMetaInfo)).thenReturn(dataObject);
        Voter<Integer> voter = mock(Voter.class);
        voters.add(voter);
        when(voter.shouldStoreOnGet(eq(newMetaInfo), (DataContainer< Integer,?>) anyObject())).thenReturn(VoteFactory.YES(voter));

        Result<Integer, String> result = dataContainer.get(newMetaInfo);

        assertEquals(newMetaInfo, result.getMetaInfo());
        assertNotNull(result.getDataObject());
        assertEquals(dataObject, result.getDataObject());
        verify(store, times(1)).store(any(Integer.class), any(String.class));
        assertNotNull(result.getVotingResult());
        assertEquals(VoteType.YES, result.getVotingResult().getHighestVote());
    }

    @Test
    public void get_shouldReturnDataObjectByFetchAndDoNotStore() {
        Integer newMetaInfo = 999;
        String dataObject = "Vola";
        when(fetcher.fetch(newMetaInfo)).thenReturn(dataObject);
        Voter<Integer> voter = mock(Voter.class);
        voters.add(voter);
        when(voter.shouldStoreOnGet(eq(newMetaInfo), (DataContainer< Integer,?>) anyObject())).thenReturn(VoteFactory.NO(voter));

        Result<Integer, String> result = dataContainer.get(newMetaInfo);

        assertEquals(newMetaInfo, result.getMetaInfo());
        assertNotNull(result.getDataObject());
        assertEquals(dataObject, result.getDataObject());
        verify(store, never()).store(any(Integer.class), any(String.class));
        assertNotNull(result.getVotingResult());
        assertEquals(VoteType.NO, result.getVotingResult().getHighestVote());
    }

    @Test
    public void get_shouldReturnDataObjectByFetchAndForceStore() {
        Integer newMetaInfo = 999;
        String dataObject = "Vola";
        when(fetcher.fetch(newMetaInfo)).thenReturn(dataObject);
        Voter<Integer> voter = mock(Voter.class);
        voters.add(voter);
        when(voter.shouldStoreOnGet(eq(newMetaInfo), (DataContainer< Integer,?>) anyObject())).thenReturn(VoteFactory.NO(voter));

        Result<Integer, String> result = dataContainer.get(newMetaInfo, true);

        assertEquals(newMetaInfo, result.getMetaInfo());
        assertNotNull(result.getDataObject());
        assertEquals(dataObject, result.getDataObject());
        verify(store, times(1)).store(any(Integer.class), any(String.class));
        assertNull(result.getVotingResult());
    }

    @Test
    public void remove_shouldReturnSomeVoteResultIfItIsNotForced() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.YES(voter));
        voters.add(voter);

        VotingResult votingResult = dataContainer.remove(metaInfo1);

        verify(voter, times(1)).shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject());
        verify(store, times(1)).remove(metaInfo1);
        assertNotNull(votingResult);
    }

    @Test
    public void remove_shouldReturnNoVoteResultIfItIsForced() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.YES(voter));
        voters.add(voter);

        VotingResult votingResult = dataContainer.remove(metaInfo1, true);

        verify(voter, never()).shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject());
        verify(store, times(1)).remove(metaInfo1);
        assertNull(votingResult);
    }

    @Test
    public void removeAll_shouldReturnVoteResultsIfItIsNotForced() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.YES(voter));
        voters.add(voter);
        List<Integer> metaInfoList = new ArrayList<Integer>();
        metaInfoList.add(metaInfo1);
        metaInfoList.add(metaInfo2);

        VotingResults votingResults = dataContainer.removeAll(metaInfoList);

        verify(voter, times(2)).shouldRemove(anyInt(), (DataContainer<Integer, String>) anyObject());
        verify(store, times(1)).removeAll(anyList());
        assertEquals(2, votingResults.getVotingResults().size());
    }

    @Test
    public void removeAll_shouldReturnVoteResultsWhichAreNullIfItIsForced() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.YES(voter));
        voters.add(voter);
        List<Integer> metaInfoList = new ArrayList<Integer>();
        metaInfoList.add(metaInfo1);
        metaInfoList.add(metaInfo2);

        VotingResults<Integer> votingResults = dataContainer.removeAll(metaInfoList, true);

        assertEquals(2, votingResults.getVotingResults().size());
        assertNull(votingResults.getVotingResults().get(metaInfo1));
        assertNull(votingResults.getVotingResults().get(metaInfo2));
    }

    @Test
    public void clear_shouldEmptyTheContainerIfVotersVoteYes() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.YES(voter));
        voters.add(voter);

        assertEquals(3, dataContainer.size());

        dataContainer.clear();

        assertEquals(0, dataContainer.size());
    }

    @Test
    public void clear_shouldNotEmptyTheContainerIfVotersVoteNo() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.NO(voter));
        voters.add(voter);

        assertEquals(3, dataContainer.size());

        dataContainer.clear();

        verify(voter, times(3)).shouldRemove(anyInt(), (DataContainer<Integer, String>) anyObject());
        verify(store, never()).removeAll(anyList());
        assertEquals(3, dataContainer.size());
    }

    @Test
    public void clear_shouldRemoveOneElementWhereItIsVotedForYes() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.NO(voter));
        when(voter.shouldRemove(eq(metaInfo2), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.YES(voter));
        voters.add(voter);

        assertEquals(3, dataContainer.size());
        assertTrue(dataContainer.getStore().containsMetaInfo(metaInfo2));

        dataContainer.clear();

        assertEquals(2, dataContainer.size());
        verify(voter, times(3)).shouldRemove(anyInt(), (DataContainer<Integer, String>) anyObject());
        verify(store, times(1)).removeAll(anyList());
        assertFalse(dataContainer.getStore().containsMetaInfo(metaInfo2));
    }

    @Test
    public void clear_shouldEmptyTheContainerIfForced() {
        Voter<Integer> voter = mock(Voter.class);
        when(voter.shouldRemove(anyInt(), (DataContainer<Integer,String>) anyObject())).thenReturn(VoteFactory.NO(voter));
        voters.add(voter);

        assertEquals(3, dataContainer.size());

        dataContainer.clear(true);

        verify(voter, never()).shouldRemove(anyInt(), (DataContainer<Integer, String>) anyObject());
        verify(store, times(1)).removeAll(anyList());
        assertEquals(0, dataContainer.size());
    }
}
