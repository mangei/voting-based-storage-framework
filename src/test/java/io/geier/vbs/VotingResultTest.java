/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class VotingResultTest {

    Voter<Integer> voter;

    @Before
    public void setUp() {
        voter = mock(Voter.class);
    }

    @After
    public void tearDown() {
        voter = null;
    }

    @Test
    public void new_shouldAllValuesBeZero() {
        VotingResult<Integer> votingResult = new VotingResult<Integer>(1);

        assertEquals(0, votingResult.getYesCount());
        assertEquals(0, votingResult.getNoCount());
        assertEquals(0, votingResult.getNoneCount());
        assertEquals(0, votingResult.getVotes().size());
        assertEquals(VoteType.NONE, votingResult.getHighestVote());
    }

    @Test
    public void count_shouldCountAllValuesCorrect() {
        VotingResult<Integer> votingResult = new VotingResult<Integer>(1);

        votingResult.count(VoteFactory.YES(voter));
        votingResult.count(VoteFactory.YES(voter));
        votingResult.count(VoteFactory.NO(voter));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));

        assertEquals(2, votingResult.getYesCount());
        assertEquals(1, votingResult.getNoCount());
        assertEquals(3, votingResult.getNoneCount());
        assertEquals(6, votingResult.getVotes().size());
    }

    @Test
    public void count_shouldCountAllValuesCorrectWithMultiplier() {
        VotingResult<Integer> votingResult = new VotingResult<Integer>(1);

        votingResult.count(VoteFactory.YES(voter, 10));
        votingResult.count(VoteFactory.YES(voter));
        votingResult.count(VoteFactory.NO(voter, 40));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));

        assertEquals(11, votingResult.getYesCount());
        assertEquals(40, votingResult.getNoCount());
        assertEquals(3, votingResult.getNoneCount());
        assertEquals(6, votingResult.getVotes().size());
    }

    @Test
    public void getHighestVote_shouldReturnHighestVote1() {
        VotingResult<Integer> votingResult = new VotingResult<Integer>(1);

        votingResult.count(VoteFactory.YES(voter, 10));
        votingResult.count(VoteFactory.YES(voter));
        votingResult.count(VoteFactory.NO(voter, 40));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));

        assertEquals(VoteType.NO, votingResult.getHighestVote());
        assertTrue(votingResult.isHighestVote(VoteType.NO));
        assertFalse(votingResult.isHighestVote(VoteType.YES));
        assertFalse(votingResult.isHighestVote(VoteType.NONE));
    }

    @Test
    public void getHighestVote_shouldReturnHighestVote2() {
        VotingResult<Integer> votingResult = new VotingResult<Integer>(1);

        votingResult.count(VoteFactory.YES(voter, 40));
        votingResult.count(VoteFactory.YES(voter));
        votingResult.count(VoteFactory.NO(voter, 40));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));

        assertEquals(VoteType.YES, votingResult.getHighestVote());
        assertFalse(votingResult.isHighestVote(VoteType.NO));
        assertTrue(votingResult.isHighestVote(VoteType.YES));
        assertFalse(votingResult.isHighestVote(VoteType.NONE));
    }

    @Test
    public void getHighestVote_shouldReturnNONEOnDraw() {
        VotingResult<Integer> votingResult = new VotingResult<Integer>(1);

        votingResult.count(VoteFactory.YES(voter, 40));
        votingResult.count(VoteFactory.YES(voter));
        votingResult.count(VoteFactory.NO(voter, 41));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));
        votingResult.count(VoteFactory.NONE(voter));

        assertEquals(VoteType.NONE, votingResult.getHighestVote());
        assertFalse(votingResult.isHighestVote(VoteType.NO));
        assertFalse(votingResult.isHighestVote(VoteType.YES));
        assertTrue(votingResult.isHighestVote(VoteType.NONE));
    }
}
