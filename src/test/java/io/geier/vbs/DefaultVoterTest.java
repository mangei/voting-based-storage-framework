/*
The MIT License (MIT)

Copyright (c) 2014 Manuel Geier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package io.geier.vbs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class DefaultVoterTest {

    DefaultVoter<Integer> defaultVoter;
    Voter<Integer> voter;
    Integer metaInfo;
    DataContainer<Integer,String> dataContainer;

    @Before
    public void setUp() {
        voter = mock(Voter.class);
        defaultVoter = new DefaultVoter<Integer>() {
            @Override
            protected Voter<Integer> getVoter() {
                return voter;
            }
        };
        metaInfo = 1;
        dataContainer = mock(DataContainer.class);
    }

    @After
    public void tearDown() {
        defaultVoter = null;
        voter = null;
        metaInfo = null;
        dataContainer = null;
    }

    @Test
    public void shouldStoreOnGet_shouldReturnNONEVote() {
        Vote vote = defaultVoter.shouldStoreOnGet(metaInfo, dataContainer);

        assertNotNull(vote);
        assertEquals(Vote.DEFAULT_MULTIPLIER, vote.getMultiplier());
        assertNull(vote.getReason());
        assertEquals(VoteType.NONE, vote.getType());
        assertEquals(voter, vote.getVoter());
    }

    @Test
    public void shouldStoreOnPreload_shouldReturnNONEVote() {
        Vote vote = defaultVoter.shouldPreload(metaInfo, dataContainer);

        assertNotNull(vote);
        assertEquals(Vote.DEFAULT_MULTIPLIER, vote.getMultiplier());
        assertNull(vote.getReason());
        assertEquals(VoteType.NONE, vote.getType());
        assertEquals(voter, vote.getVoter());
    }

    @Test
    public void shouldUpdate_shouldReturnNONEVote() {
        Vote vote = defaultVoter.shouldUpdate(metaInfo, dataContainer);

        assertNotNull(vote);
        assertEquals(Vote.DEFAULT_MULTIPLIER, vote.getMultiplier());
        assertNull(vote.getReason());
        assertEquals(VoteType.NONE, vote.getType());
        assertEquals(voter, vote.getVoter());
    }

    @Test
    public void shouldRemove_shouldReturnNONEVote() {
        Vote vote = defaultVoter.shouldRemove(metaInfo, dataContainer);

        assertNotNull(vote);
        assertEquals(Vote.DEFAULT_MULTIPLIER, vote.getMultiplier());
        assertNull(vote.getReason());
        assertEquals(VoteType.NONE, vote.getType());
        assertEquals(voter, vote.getVoter());
    }
}
