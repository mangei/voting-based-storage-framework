# Voting-based Storage Framework

by Manuel Geier (http://geier.io)

# Build

Build the framework:  
`gradlew build`

You can find the built .jar under `build/libs`.

# Javadoc

To generate the javadoc use:  
`gradlew javadoc`

You can find the javadoc under `build/docs`.

# IntelliJ IDEA

To create project files use:  
`gradlew idea`

Open the project files with IntelliJ IDEA.

# Eclipse

To create project files use:  
`gradlew eclipse`

Open the project files with Eclipse.
